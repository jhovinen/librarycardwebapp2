import {Component, OnInit} from '@angular/core';
import * as X2JS from '../../../node_modules/x2js/x2js';
import {LibraryCard} from '../library-card';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {isUndefined} from 'util';
import {Book} from '../book';

@Component({
  selector: 'app-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.css']
})
export class HomeViewComponent implements OnInit {

  model: LibraryCard = new LibraryCard();
  newModel: LibraryCard = new LibraryCard();
  searchModel: LibraryCard = new LibraryCard();
  libraryCards: Array<LibraryCard> = [];
  addBookBarcode: String;
  dateOfReturn: String;
  initialDateOfReturn: String;
  searchError: string;
  addLibraryCardError: string;
  libraryCardError: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
  }

  onAddLibraryCardSubmit() {
    const name = this.newModel.name === undefined ? '' : this.newModel.name;
    const surname = this.newModel.surname === undefined ? '' : this.newModel.surname;
    const userId = this.newModel.userId === undefined ? '' : this.newModel.userId;
    const body =
    `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.ttu.ee/idu0075/library-card-service/1.0">
     <soapenv:Header/>
     <soapenv:Body>
        <ns:addLibraryCardRequest>
           <ns:userName>${name}</ns:userName>
           <ns:userSurname>${surname}</ns:userSurname>
           <ns:userID>${userId}</ns:userID>
           <ns:token>secret</ns:token>
        </ns:addLibraryCardRequest>
     </soapenv:Body>
  </soapenv:Envelope>`;

    this.http.post('http://localhost:8080/LibraryCardWebApp/libraryCardService', body, {
      headers: new HttpHeaders().set('Content-Type', 'text/xml').append('Accept', 'text/xml; '), responseType: 'text'
    }).subscribe((xml) => {
      if ( !xml ) {
        console.log(xml);
        return;
      }
      const x2js = new X2JS();
      const obj = x2js.xml2js<any>(xml);
      if (obj && obj.Envelope && obj.Envelope.Body && obj.Envelope.Body.addLibraryCardResponse) {
        const path =  obj.Envelope.Body.addLibraryCardResponse;
        const lc = path.libraryCard;
        const err = path.error;
          if (lc) {
            this.model.update(lc.ID, lc.userID, lc.userName, lc.userSurname, []);
            this.addLibraryCardError = '';
          } else {
            this.addLibraryCardError = err;
          }
        this.libraryCards = [];
      }
      console.log(obj);
    });
    console.log('Submit');
  }
  onSearchSubmit() {
    const name = this.searchModel.name === undefined ? '' : this.searchModel.name;
    const surname = this.searchModel.surname === undefined ? '' : this.searchModel.surname;
    const userId = this.searchModel.userId === undefined ? '' : this.searchModel.userId;
    const id = this.searchModel.id === undefined ? '' : this.searchModel.id;
    const body =
      `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.ttu.ee/idu0075/library-card-service/1.0">
     <soapenv:Header/>
     <soapenv:Body>
        <ns:findLibraryCardRequest>
           <ns:ID>${id}</ns:ID>
           <ns:userName>${name}</ns:userName>
           <ns:userSurname>${surname}</ns:userSurname>
           <ns:userID>${userId}</ns:userID>
           <ns:token>secret</ns:token>
        </ns:findLibraryCardRequest>
     </soapenv:Body>
  </soapenv:Envelope>`;

    this.http.post('http://localhost:8080/LibraryCardWebApp/libraryCardService', body, {
      headers: new HttpHeaders().set('Content-Type', 'text/xml').append('Accept', 'text/xml; '), responseType: 'text'
    }).subscribe((xml) => {
      if ( !xml ) {
        console.log(xml);
        return;
      }
      const x2js = new X2JS();
      const obj = x2js.xml2js<any>(xml);
      if (obj && obj.Envelope && obj.Envelope.Body && obj.Envelope.Body.findLibraryCardResponse) {
        const path =  obj.Envelope.Body.findLibraryCardResponse;
        const lcs = path.libraryCards.libraryCard;
        const err = path.error;
        let libraryCard;
        let books;
        this.libraryCards = [];
        if (lcs && lcs.length > 0) {
          lcs.forEach(lc => {
            libraryCard = new LibraryCard();
            books = [];
            if (lc.books && lc.books.book) {
              if (lc.books.book.length) {
                lc.books.book.forEach(b => {
                  books.push(new Book(b.barcode, b.name, b.author, b.owner, b.dayOfReturn));
                });
              } else {
                books.push(new Book(lc.books.book.barcode, lc.books.book.name,
                  lc.books.book.author, lc.books.book.owner, lc.books.book.dayOfReturn));
              }
            }
            libraryCard.update(lc.ID, lc.userID, lc.userName, lc.userSurname, books);
            this.libraryCards.push(libraryCard);
          });
          this.searchError = '';
        } else if (lcs) {
          const lc = lcs;
          libraryCard = new LibraryCard();
          books = [];
          if (lc.books && lc.books.book) {
            if (lc.books.book.length) {
              lc.books.book.forEach(b => {
                books.push(new Book(b.barcode, b.name, b.author, b.owner, b.dayOfReturn));
              });
            } else {
              books.push(new Book(lc.books.book.barcode, lc.books.book.name,
                lc.books.book.author, lc.books.book.owner, lc.books.book.dayOfReturn));
            }
          }
          libraryCard.update(lc.ID, lc.userID, lc.userName, lc.userSurname, books);
          this.libraryCards.push(libraryCard);
        } else {
          this.searchError = err;
        }
      }
      console.log(obj);
    });
    this.model.reset();
    console.log('Submit');
  }
  selectLibraryCard(lc) {
    this.model.update(lc.id, lc.userId, lc.name, lc.surname, lc.books);
    this.libraryCards = [];
  }
  deleteBook(b) {
    const body =
      `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns="http://www.ttu.ee/idu0075/library-card-service/1.0">
     <soapenv:Header/>
     <soapenv:Body>
        <ns:removeBookRequest>
           <ns:libraryCardID>${b.owner}</ns:libraryCardID>
           <ns:bookBarcode>${b.barcode}</ns:bookBarcode>
           <ns:token>secret</ns:token>
        </ns:removeBookRequest>
     </soapenv:Body>
  </soapenv:Envelope>`;

    this.http.post('http://localhost:8080/LibraryCardWebApp/libraryCardService', body, {
      headers: new HttpHeaders().set('Content-Type', 'text/xml').append('Accept', 'text/xml; '), responseType: 'text'
    }).subscribe((xml) => {
      if ( !xml ) {
        console.log(xml);
        return;
      }
      const x2js = new X2JS();
      const obj = x2js.xml2js<any>(xml);
      if (obj && obj.Envelope && obj.Envelope.Body && obj.Envelope.Body.removeBookResponse) {
        const path =  obj.Envelope.Body.removeBookResponse;
        const lc = path.libraryCard;
        const err = path.error;
        let books;
        if (lc) {
          books = [];
          this.model.reset();
          if (lc.books && lc.books.book) {
            if (lc.books.book.length) {
              lc.books.book.forEach( b => {
                books.push(new Book(b.barcode, b.name, b.author, b.owner, b.dayOfReturn));
              });
            } else {
              books.push(new Book(lc.books.book.barcode, lc.books.book.name,
                lc.books.book.author, lc.books.book.owner, lc.books.book.dayOfReturn));
            }
          }
          this.model.update(lc.ID, lc.userID, lc.userName, lc.userSurname, books);
          this.libraryCardError = '';
        } else {
          this.libraryCardError = err;
        }
      }
      console.log(obj);
    });
    console.log('Submit');
  }
  onDateUpdateSubmit() {
    const x2js = new X2JS({
      datetimeAccessFormPaths : [
        'Body.extendTimeRequest.date' /* Configure it beforehand */
      ]
    });
    const bookBarcode = [];
    if (this.model.books && this.model.books.length) {
      this.model.books.forEach(b => {
        bookBarcode.push({
          __prefix: 'ns',
          __text: b.barcode
        });
      });
    }
    const testObjNew = {
      Envelope : {
        __prefix : 'soapenv',
        '_xmlns:soapenv' : 'http://schemas.xmlsoap.org/soap/envelope/',
        '_xmlns:ns' : 'http://www.ttu.ee/idu0075/library-card-service/1.0',
        Header: {
          __prefix: 'soapenv'
        },
        Body : {
          __prefix: 'soapenv',
          extendTimeRequest : {
            __prefix : 'ns',
            libraryCardID: {
              __prefix : 'ns',
              __text : this.model.id
            },
            bookBarcodeList: {
              __prefix : 'ns',
              bookBarcode
            },
            token: {
              __prefix: 'ns',
              __text: 'secret'
            },
            date : {
              __prefix: 'ns',
              __text: this.dateOfReturn
            }
          }
        }
      }
    };
    const body = x2js.js2xml<any>(testObjNew);
    console.log(body);
    this.http.post('http://localhost:8080/LibraryCardWebApp/libraryCardService', body, {
      headers: new HttpHeaders().set('Content-Type', 'text/xml').append('Accept', 'text/xml; '), responseType: 'text'
    }).subscribe((xml) => {
      this.addBookBarcode = '';
      this.initialDateOfReturn = '';
      if ( !xml ) {
        console.log(xml);
        return;
      }
      const x2js2 = new X2JS();
      const obj = x2js2.xml2js<any>(xml);
      if (obj && obj.Envelope && obj.Envelope.Body && obj.Envelope.Body.extendTimeResponse) {
        const path =  obj.Envelope.Body.extendTimeResponse;
        console.log(path);
        const lc = path.libraryCard;
        const err = path.error;
        let books;
        if (lc) {
          books = [];
          this.model.reset();
          if (lc.books && lc.books.book) {
            if (lc.books.book.length) {
              lc.books.book.forEach( b => {
                books.push(new Book(b.barcode, b.name, b.author, b.owner, b.dayOfReturn));
              });
            } else {
              books.push(new Book(lc.books.book.barcode, lc.books.book.name,
                lc.books.book.author, lc.books.book.owner, lc.books.book.dayOfReturn));
            }
          }
          this.model.update(lc.ID, lc.userID, lc.userName, lc.userSurname, books);
          this.libraryCardError = '';
        } else {
          this.libraryCardError = err;
        }
      }
      console.log(obj);
    });
    console.log('Submit');
  }
  onAddBookSubmit() {
    const barcode = this.addBookBarcode === undefined ? '' : this.addBookBarcode;
    const x2js = new X2JS({
      datetimeAccessFormPaths : [
        'Body.addBookRequest.date' /* Configure it beforehand */
      ]
    });
    const testObjNew = {
      Envelope : {
        __prefix : 'soapenv',
        '_xmlns:soapenv' : 'http://schemas.xmlsoap.org/soap/envelope/',
        '_xmlns:ns' : 'http://www.ttu.ee/idu0075/library-card-service/1.0',
        Header: {
          __prefix: 'soapenv'
        },
        Body : {
          __prefix: 'soapenv',
          addBookRequest : {
            __prefix : 'ns',
            libraryCardID: {
              __prefix : 'ns',
              __text : this.model.id
            },
            bookBarcodeList: {
              __prefix : 'ns',
              bookBarcode : {
                __prefix: 'ns',
                __text: barcode
              }
            },
            token: {
              __prefix: 'ns',
              __text: 'secret'
            },
            date : {
              __prefix: 'ns',
              __text: this.initialDateOfReturn
            }
          }
        }
      }
    };
    const body = x2js.js2xml<any>(testObjNew);
    this.http.post('http://localhost:8080/LibraryCardWebApp/libraryCardService', body, {
      headers: new HttpHeaders().set('Content-Type', 'text/xml').append('Accept', 'text/xml; '), responseType: 'text'
    }).subscribe((xml) => {
      this.addBookBarcode = '';
      this.initialDateOfReturn = '';
      if ( !xml ) {
        console.log(xml);
        return;
      }
      const x2js2 = new X2JS();
      const obj = x2js2.xml2js<any>(xml);
      if (obj && obj.Envelope && obj.Envelope.Body && obj.Envelope.Body.addBookResponse) {
        const path =  obj.Envelope.Body.addBookResponse;
        const lc = path.libraryCard;
        const err = path.error;
        let books;
        if (lc) {
          books = [];
          this.model.reset();
          if (lc.books && lc.books.book) {
            if (lc.books.book.length) {
              lc.books.book.forEach( b => {
                books.push(new Book(b.barcode, b.name, b.author, b.owner, b.dateOfReturn));
              });
            } else {
              books.push(new Book(lc.books.book.barcode, lc.books.book.name,
                lc.books.book.author, lc.books.book.owner, lc.books.book.dateOfReturn));
            }
          }
          this.model.update(lc.ID, lc.userID, lc.userName, lc.userSurname, books);
          this.libraryCardError = '';
        } else {
          this.libraryCardError = err;
        }
      }
      console.log(obj);
    });
    console.log('Submit');
  }
}
