export class Book {
  public barcode: String;
  public author: String;
  public name: String;
  public owner: String;
  public dateOfReturn: String;
  constructor( barcode, name, author, owner, dateOfReturn) {
    this.barcode = barcode;
    this.name = name;
    this.author = author;
    this.owner = owner;
    this.dateOfReturn = dateOfReturn;
    console.log(this);
  }
}
