import {Book} from "./book";
export class LibraryCard {
  public id: String;
  public userId: String;
  public name: String;
  public surname: String;
  public books: Array<Book>;

  update( resultId , resultUserId, resultName, resultSurname, books) {
      this.id = resultId;
      this.userId = resultUserId;
      this.name = resultName;
      this.surname = resultSurname;
      this.books = books;
  }
  reset() {
    this.id = null;
    this.userId = null;
    this.name = null;
    this.surname = null;
    this.books = null;
  }
}
